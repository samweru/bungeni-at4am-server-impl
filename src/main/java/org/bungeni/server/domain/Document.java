package org.bungeni.server.domain;

import java.io.Serializable;
import java.util.Calendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Date: 12/03/13 11:53
 *
 * @author <a href="philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="document")
public class Document implements Serializable{

	@XmlElement(name="Id")
    private Long ID;

    /**
     * public key.
     */
	@XmlElement(name="documentId")
    private String documentID;


	@XmlElement(name="creationDate")
    private Calendar creationDate;

	@XmlElement(name="modificationDate")
    private Calendar modificationDate;

    /**
     * The name of the document.
     */
	@XmlElement(name="documentName")
    private String name;
    /**
     * The 2 letter ISO code for this translation.
     */
	@XmlElement(name="languageIso")
    private String languageIso;

    /**
     * A flag indicating if this document is amendable or not.
     */
	@XmlElement(name="amendable")
    private boolean amendable;

    /**
     * The deadline for this document.
     */
	@XmlElement(name="deadline")
    private Calendar deadline;
	
	@XmlElement(name="content")
	private String content;

    public Document() {
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public Calendar getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Calendar modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguageIso() {
        return languageIso;
    }

    public void setLanguageIso(String languageIso) {
        this.languageIso = languageIso;
    }

    public boolean isAmendable() {
        return amendable;
    }

    public void setAmendable(boolean amendable) {
        this.amendable = amendable;
    }

    public Calendar getDeadline() {
        return deadline;
    }

    public void setDeadline(Calendar deadline) {
        this.deadline = deadline;
    }
    
    public void setContent(String content){
    	this.content=content;
    }
    
    public String getContent(){
    	return content;
    }
}