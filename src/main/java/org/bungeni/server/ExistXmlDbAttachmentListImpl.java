package org.bungeni.server;

import java.util.Properties;

import org.bungeni.server.properties.domain.RESTProperties;
import org.bungeni.server.properties.repository.RESTPropertiesRepository;
import org.bungeni.server.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExistXmlDbAttachmentListImpl {

	private RequestBuilder rBuilder;

	private String url;
	private String method;
	
	private final Logger logger = LoggerFactory.getLogger(ExistXmlDbAttachmentListImpl.class);
	
	public ExistXmlDbAttachmentListImpl() {
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod(method);
	}
		
	public ExistXmlDbAttachmentListImpl(String searchStr) {

		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod(method);
		
		setSearchString(searchStr);
	}
	
	public ExistXmlDbAttachmentListImpl(String searchStr, String pageStr, String strPerPage) {

		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod("POST");
		
		if(!searchStr.equals(""))
			setSearchString(searchStr);
		
		if(!strPerPage.equals(""))
			setPerPage(strPerPage);	
		
		if(!pageStr.equals(""))
			setPage(pageStr);
	}
	
	private void getProperties(){

        RESTProperties restProps = new RESTProperties(RESTPropertiesRepository.getProperties());

        url=restProps.getAttachmentListPath();
        method=restProps.getMethod();
	}
	
	public RequestBuilder getRequestBuilder(){
		
		return rBuilder;
	}
	
	public void setSearchString(String searchStr){
		
		rBuilder.addParameter("search=".concat(searchStr));
	}
	
	public void setPage(String page){
		
		rBuilder.addParameter("page=".concat(page));
	}

	public void setPerPage(String perPage){
	
		rBuilder.addParameter("perPage=".concat(perPage));
	}
	
	public String getListAsString(){
		
		try{
			
			logger.info("**Response");
			logger.info(rBuilder.toString());
			
			rBuilder.doRequest();
			
			return rBuilder.getResponse();
		}
		catch(Exception e){
			
			logger.info("**error");
			logger.error(e.getMessage());
			
			e.printStackTrace();
			
			return null;
		}
	}
}
