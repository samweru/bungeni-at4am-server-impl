package org.bungeni.server;

import java.util.Properties;

import org.bungeni.server.properties.domain.RESTProperties;
import org.bungeni.server.properties.repository.RESTPropertiesRepository;
import org.bungeni.server.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExistXmlDbAmendmentListImpl {

	private RequestBuilder rBuilder;

	private String url;
	private String method;
	
	private final Logger logger = LoggerFactory.getLogger(ExistXmlDbAmendmentListImpl.class);
	
	public ExistXmlDbAmendmentListImpl(){
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod(method);
	}
	
	public ExistXmlDbAmendmentListImpl(String attachmentFileName){
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod(method);
		
		this.setAttachmentFileName(attachmentFileName);
	}
	
	private void getProperties(){

        RESTProperties restProps = new RESTProperties(RESTPropertiesRepository.getProperties());

        url=restProps.getAttachmentListPath();
        method=restProps.getMethod();
	}
	
	public void setAttachmentFileName(String attachmentFileName){
		
		rBuilder.addParameter("filename=".concat(attachmentFileName));
	}
	
	public String getListAsString(){
		
		try{
			
			logger.info("**Response");
			logger.info(rBuilder.toString());
			
			rBuilder.doRequest();
			
			return rBuilder.getResponse();
		}
		catch(Exception e){
			
			logger.info("**Error");
			logger.error(e.getMessage());
			
			e.printStackTrace();
			
			return null;
		}
	}
}
