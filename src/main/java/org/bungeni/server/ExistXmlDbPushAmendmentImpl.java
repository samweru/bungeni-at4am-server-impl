package org.bungeni.server;

import java.util.Properties;

import org.bungeni.server.properties.domain.RESTProperties;
import org.bungeni.server.properties.repository.RESTPropertiesRepository;
import org.bungeni.server.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExistXmlDbPushAmendmentImpl {

	private RequestBuilder rBuilder;

	private String url;
	private String method;
	
	private String username;
	private String password;
	
	private String document;
	private String filename;
	
	private final Logger logger = LoggerFactory.getLogger(ExistXmlDbPushAmendmentImpl.class);
	
	public ExistXmlDbPushAmendmentImpl(){
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setMethod(method);
		rBuilder.setServiceUrl(url);
	}
	
	private void getProperties(){

        RESTProperties restProps = new RESTProperties(RESTPropertiesRepository.getProperties(), RESTPropertiesRepository.getAuthProperties());

        url=restProps.getAmendmentSavePath();
        method=restProps.getMethod();
		username = restProps.getUsername();
		password = restProps.getPassword();
	}
	
	public RequestBuilder getRequestBuilder(){
		
		return rBuilder;
	}
	
	public void setDocument(String filename, String document){
		
		this.document = document;
		this.filename = filename;
	}
	
	public String push(){
		
		String urlParams = ""; 
		urlParams += "username=".concat(username);
		urlParams += "&password=".concat(password);
		urlParams += "&filename=".concat(filename);
		urlParams += "&document=".concat(document);
		
		try{
			
			logger.info("**Reponse");
			logger.info(rBuilder.toString());
			rBuilder.addParameter(urlParams);
			rBuilder.doRequest();
			
			return rBuilder.getResponse();
		}
		catch(Exception e){
			
			logger.info("**Error");
			logger.error(e.getMessage());
			
			e.printStackTrace();
			
			return null;
		}
	}
	
	public String toString(){
		
		String urlParams = ""; 
		urlParams += "username=".concat(username);
		urlParams += "&password=".concat(password);
		urlParams += "&filename=".concat(filename);
		urlParams += "&document=".concat(document);
		
		return url+"?".concat(urlParams);
	}
}

