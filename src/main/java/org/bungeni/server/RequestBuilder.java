package org.bungeni.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestBuilder{
	
	private String method; 
	private String serviceUrl;
	private String urlParams = "";
	
	private String response = null;
	
	private boolean doOutput = true;
	private boolean doInput = true;
	
	private Map<String, String> headersProperties = null;
	
	private final Logger logger = LoggerFactory.getLogger(RequestBuilder.class);
	
	public RequestBuilder(){
		
		this.method = "GET";
	}
	
	public RequestBuilder(String method, String serviceUrl, String urlParams ){
		
		this.method = method;
		this.serviceUrl = serviceUrl;
		this.urlParams = urlParams;
	}
	
	public RequestBuilder(String method, String serviceUrl){
		
		this.method = method;
		this.serviceUrl = serviceUrl;
	}
	
	public void setDoOutput(boolean doOutput){
		
		this.doOutput = doOutput;
	}
	
	public void setDoInput(boolean doInput){
		
		this.doInput = doInput;
	}
	
	public void setMethod(String method){
		
		this.method = method;
	}
	
	public void setServiceUrl(String serviceUrl){
			
			this.serviceUrl = serviceUrl;
		}
	
	public void addParameter(String urlParam){
		
		if(this.urlParams.equals(""))
			this.urlParams = urlParam;
		else
			this.urlParams += "&"+urlParam;	
	}
	
	public void addHeaderProperty(String key, String val){
		
		headersProperties  = new HashMap<String, String>();
		headersProperties.put(key, val);
	}
	
	public String getResponse(){
		
		return response;
	}
	
	public void doRequest(){
		
		//Do request atleast ten times before failling
		doRequest(10);
	}
	
	public void doRequest(int numTries){
		
		int triesAtStart=numTries;
		while(numTries-- != 0){
			
			logger.info("**Iteration: "+(triesAtStart-numTries));
			try {
			   
				doRealRequest();
		        break;
			} 
			catch(Exception e) {
			   
				if(numTries==0){
					
					logger.info("**error");
					logger.info(e.getMessage());
				}
				continue;
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void doRealRequest() throws MalformedURLException, IOException, Exception {
		
		URL url = new URL(serviceUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		
		if(headersProperties!=null){
			
			@SuppressWarnings("rawtypes")
			Iterator it = headersProperties.entrySet().iterator();
		    while (it.hasNext()) {
		    	
		        Map.Entry headerProperty = (Map.Entry)it.next();
		        logger.info("**Header: "+headerProperty.getKey() + " = " + headerProperty.getValue());
		        conn.addRequestProperty(headerProperty.getKey().toString(), headerProperty.getValue().toString());
		    }
		}
		
        conn.setDoInput(doInput);
        conn.setRequestMethod(method);
        conn.setUseCaches(false);
        conn.setDoOutput(doOutput);
        
        if(urlParams!=null){
            
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");  
            writer.write(urlParams);  
            writer.flush();
            writer.close();  
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader((conn.getInputStream())));
 
		String output = "";
		StringBuffer response = new StringBuffer(); 
		while ((output = reader.readLine()) != null) {
            
			response.append(output);
		    response.append('\r');
		}
		reader.close();
		
		this.response = response.toString();
	
		if(conn != null) 
			conn.disconnect();
	}
	
	public String toString(){
		
		return "**METHOD: "+method+
				" URL: "+serviceUrl+
				" PARAMS: "+urlParams;
	}
}