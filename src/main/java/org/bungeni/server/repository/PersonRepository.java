package org.bungeni.server.repository;

import org.bungeni.server.domain.Person;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 4/10/13
 * Time: 4:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class PersonRepository {

    public static Person findByPersonID(String Id){

        Person dummyUser = new Person();
        dummyUser.setID(Long.parseLong("34598734952"));
        dummyUser.setPersonID(Id);
        dummyUser.setUsername("dummyUser");
        dummyUser.setName("dumyUser");
        dummyUser.setLastName("User");

        return dummyUser;
    }

    public static Person findByUsername(String username){

        Person dummyUser = new Person();
        dummyUser.setID(Long.parseLong("098456847558"));
        dummyUser.setPersonID(UUID.randomUUID().toString());
        dummyUser.setUsername(username);
        dummyUser.setName("dumyUser");
        dummyUser.setLastName("User");

        return dummyUser;
    }
}
