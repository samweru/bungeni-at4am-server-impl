package org.bungeni.server.repository;

import org.bungeni.server.domain.DocumentContent;
import org.springframework.stereotype.Repository;

/**
 * Date: 12/03/13 13:53
 *
 * @author <a href="philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
@Repository
public interface DocumentContentRepository{

    DocumentContent findByDocumentID(String documentID);
}
