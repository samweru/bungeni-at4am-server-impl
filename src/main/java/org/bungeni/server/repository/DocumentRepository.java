package org.bungeni.server.repository;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.bungeni.server.ExistXmlDbAttachmentListImpl;
import org.bungeni.server.ExistXmlDbPullAttachmentImpl;
import org.bungeni.server.domain.Document;

public class DocumentRepository {
	
	public static Document findDocumentById(String Id){
		
		Document d = new Document();
		d.setAmendable(true);
		d.setCreationDate(GregorianCalendar.getInstance());
        final Calendar deadline = GregorianCalendar.getInstance();
        deadline.set(Calendar.HOUR_OF_DAY, 12);
        d.setDeadline(deadline);
        d.setAmendable(true);
        d.setLanguageIso("EN");
        d.setDocumentID(Id);
		
		return d;
	}
	
	public static String getAttachmentById(String Id){
		
		ExistXmlDbPullAttachmentImpl attachment = new ExistXmlDbPullAttachmentImpl(Id);
    	String xmlAttachment=attachment.pull();
    	
    	return xmlAttachment;
	}
	
	public static String getPagedAttachmentList(String searchText, String page, String recordsPerPage){
		
		ExistXmlDbAttachmentListImpl list = new ExistXmlDbAttachmentListImpl(searchText, page, recordsPerPage);
		String xmlDocListStr = list.getListAsString();
		
		return xmlDocListStr;
	}
}
