package org.bungeni.server.repository;

import org.bungeni.server.ExistXmlDbAmendmentListImpl;
import org.bungeni.server.ExistXmlDbPopAmendmentImpl;
import org.bungeni.server.ExistXmlDbPushAmendmentImpl;
import org.bungeni.server.domain.AmendableWidgetReference;
import org.bungeni.server.domain.AmendmentAction;
import org.bungeni.server.domain.AmendmentContainer;
import org.bungeni.server.util.Util;
import org.bungeni.server.dto.AmendmentContainerDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AmendmentContainerRepository {
	
	static Logger logger = LoggerFactory.getLogger(AmendmentContainerRepository.class);

	public static List<AmendmentContainer> getAmendmentListByDocumentId(String documentId){
		
		ExistXmlDbAmendmentListImpl amendments = new ExistXmlDbAmendmentListImpl(documentId);
		String xmlResponseString=amendments.getListAsString();

        List<AmendmentContainer> amendmentConatiners = new ArrayList<AmendmentContainer>();
        NodeList nodeList = null;

        try {
            nodeList = Util.getNodeList("/amendments/amendmentDocument", xmlResponseString);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (SAXException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (XPathExpressionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        for(int i=0; i<((NodeList) nodeList).getLength(); i++){

            //AmendmentContainerDTO amendmentDto = new AmendmentContainerDTO();
            Node nNode = ((NodeList) nodeList).item(i);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;

                String ref = eElement.getElementsByTagName("ref").item(0).getTextContent();
                String name = eElement.getElementsByTagName("name").item(0).getTextContent();
                String document = Util.nodeToString(eElement.getElementsByTagName("akomaNtoso").item(0));

                logger.info("**Amendment Reference: " + ref);
                logger.info("**Amendment Name: " + name);
                logger.info("**Amendment Document: " + document);

                //previous uuid
                String strUuid=name.replace("-am.xml", "");

                AmendmentContainer amendmentContainer = new AmendmentContainer();

                amendmentContainer.setAmendmentContainerID(strUuid);
                amendmentContainer.setRevisionID(UUID.randomUUID().toString());
                amendmentContainer.setBody(document);
                amendmentContainer.setAmendmentAction(AmendmentAction.MODIFICATION);
                AmendableWidgetReference reference = new AmendableWidgetReference(ref);
                amendmentContainer.setSourceReference(reference);

                amendmentConatiners.add(amendmentContainer);
            }
        }
		
		return amendmentConatiners;
	}

    public static void save(AmendmentContainerDTO amendmentContainer){

        String documentBody=amendmentContainer.getBody();
        String filename=amendmentContainer.getAmendmentContainerID().concat("-am.xml");

        ExistXmlDbPushAmendmentImpl attachment = new ExistXmlDbPushAmendmentImpl();
        attachment.setDocument(filename, documentBody);
        String response = attachment.push();

        logger.info("**save");
        logger.info(response);
    }
	
	public static void delete(String filename){
		
		ExistXmlDbPopAmendmentImpl amendment = new ExistXmlDbPopAmendmentImpl(filename);
		String response = amendment.pop();
		
		logger.info("**delete");
		logger.info(response);
	}
}
