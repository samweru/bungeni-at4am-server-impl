package org.bungeni.server;

import java.util.Properties;

import org.bungeni.server.properties.domain.RESTProperties;
import org.bungeni.server.properties.repository.RESTPropertiesRepository;
import org.bungeni.server.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExistXmlDbPopAmendmentImpl {

	private RequestBuilder rBuilder;

	private String url;
	private String method;
	
	private String username;
	private String password;
	private String filename;
	
	private final Logger logger = LoggerFactory.getLogger(ExistXmlDbPopAmendmentImpl.class);
	
	public ExistXmlDbPopAmendmentImpl() {
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setMethod(method);
	}
		
	public ExistXmlDbPopAmendmentImpl(String name) {
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setMethod(method);
		setFilename(name);
	}
	
	public ExistXmlDbPopAmendmentImpl(String name, String method) {
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setMethod(method);
		setFilename(name);
	}
	
	private void getProperties(){

        RESTProperties restProps = new RESTProperties(RESTPropertiesRepository.getProperties(), RESTPropertiesRepository.getAuthProperties());

        url=restProps.getAmendmentDeletePath();
        method=restProps.getMethod();
        username = restProps.getUsername();
        password = restProps.getPassword();
	}
	
	public RequestBuilder getRequestBuilder(){
		
		return rBuilder;
	}
	
	public void setUsername(String username){
		
		this.username=username;
	}
	
	public void setPassword(String password){
		
		this.password=password;
	}
	
	public void setFilename(String filename){
		
		this.filename=filename;
	}
	
	public String pop(){
		 
		String urlParams = "username=".concat(username);
		urlParams += "&password=".concat(password);
		urlParams += "&filename=".concat(filename);
		
		try{

			logger.info("**Response");
			rBuilder.setServiceUrl(url);
			rBuilder.addParameter(urlParams);
			rBuilder.doRequest();
			logger.info(rBuilder.toString());
			
			return rBuilder.getResponse();
		}
		catch(Exception e){
			
			logger.info("**error");
			logger.error(e.getMessage());
			
			e.printStackTrace();
			
			return null;
		}
	}
	
	public String toString(){
		
		String urlParams = ""; 
		urlParams += "username=".concat(username);
		urlParams += "&password=".concat(password);
		urlParams += "&filename=".concat(filename);
		
		return url+"?".concat(urlParams);
	}
}
