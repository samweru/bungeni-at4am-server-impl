package org.bungeni.server;

import org.bungeni.server.properties.domain.APIProperties;
import org.bungeni.server.properties.repository.APIPropertiesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 4/17/13
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class BungeniAPIImpl {

    private String apiUrlWorkSpaceSections;
    private String apiUrlWorkSpaceInbox;
    private String apiUrlUserList;
    private String apiUrlCurrentUser;

    private final Logger logger = LoggerFactory.getLogger(BungeniAPIImpl.class);

    public class APIRequest{

        private RequestBuilder rb = new RequestBuilder();

        public void method(String method){

            rb.setMethod(method);
        }

        public void url(String url){

            rb.setServiceUrl(url);
        }

        public void accessToken(String accessToken){

            rb.addHeaderProperty("Authorization", "Bearer ".concat(accessToken));
        }

        public void param(String key, String val){

            rb.addParameter(key+"="+val);
        }

        public String go(){

            try {
                rb.doRealRequest();
            } catch (Exception e) {

                logger.error(e.getMessage());
            }

            logger.info("**Request Details: "+rb.toString());
            logger.info("**Response Details: "+rb.getResponse());

            return  rb.getResponse();
        }
    }

    public BungeniAPIImpl(){

        getProperties();
    }

    private void getProperties(){

        APIProperties apiProps = new APIProperties(APIPropertiesRepository.getProperties());

        apiUrlWorkSpaceSections = apiProps.getUrlWorkSpaceSections();
        apiUrlWorkSpaceInbox = apiProps.getUrlWorkspaceInbox();
        apiUrlUserList = apiProps.getUrlUserList();
        apiUrlCurrentUser = apiProps.getUrlCurrentUser();
    }

    public String getInboxItemDetails(String accessToken, String objectId){

        APIRequest apiRq = new APIRequest();
        apiRq.url(apiUrlWorkSpaceInbox.concat("/"+objectId));
        apiRq.accessToken(accessToken);
        apiRq.param("filter_status","received");
        return apiRq.go();
    }


    public String getInboxItemAttachmentsDetails(String accessToken, String objectId){

        APIRequest apiRq = new APIRequest();
        apiRq.url(apiUrlWorkSpaceInbox.concat("/"+objectId+"files"));
        apiRq.accessToken(accessToken);
        //apiRq.param("filter_status","received");
        return apiRq.go();
    }

    public String getInboxItemAttachmentDetails(String accessToken, String objectId, String ObjectId2){

        APIRequest apiRq = new APIRequest();
        apiRq.url(apiUrlWorkSpaceInbox.concat("/"+objectId+"files/"+ObjectId2));
        apiRq.accessToken(accessToken);
        return apiRq.go();
    }

    public String getWorkspaceSections(String accessToken){

        APIRequest apiRq = new APIRequest();
        apiRq.url(apiUrlWorkSpaceSections);
        apiRq.accessToken(accessToken);
        return apiRq.go();
    }

    public String getWorkspaceFilteredInboxByStatus(String accessToken, String status){

        APIRequest apiRq = new APIRequest();
        apiRq.url(apiUrlWorkSpaceInbox);
        apiRq.param("filter_status", status);
        apiRq.accessToken(accessToken);
        return apiRq.go();
    }

    public String getUsersList(String accessToken){

        APIRequest apiRq = new APIRequest();
        apiRq.url(apiUrlUserList);
        apiRq.accessToken(accessToken);
        return apiRq.go();
    }

    public String getCurrentUser(String accessToken){

        APIRequest apiRq = new APIRequest();
        apiRq.url(apiUrlCurrentUser);
        apiRq.accessToken(accessToken);
        return apiRq.go();
    }
}
