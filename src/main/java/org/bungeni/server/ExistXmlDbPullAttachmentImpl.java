package org.bungeni.server;

import java.util.Properties;

import org.bungeni.server.properties.domain.RESTProperties;
import org.bungeni.server.properties.repository.RESTPropertiesRepository;
import org.bungeni.server.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExistXmlDbPullAttachmentImpl {

	private RequestBuilder rBuilder;

	private String url;
	private String method;

	Logger logger = LoggerFactory.getLogger(ExistXmlDbPullAttachmentImpl.class);
	
	public ExistXmlDbPullAttachmentImpl() {
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod(method);
	}
		
	public ExistXmlDbPullAttachmentImpl(String name) {
		
		getProperties();
		
		rBuilder = new RequestBuilder();
		rBuilder.setServiceUrl(url);
		rBuilder.setMethod(method);
		setDocumentName(name);
	}
	
	private void getProperties(){

        RESTProperties restProps = new RESTProperties(RESTPropertiesRepository.getProperties());

        url=restProps.getAttachmentPath();
        method=restProps.getMethod();
	}
	
	public RequestBuilder getRequestBuilder(){
		
		return rBuilder;
	}
	
	public void setDocumentName(String name){
		
		rBuilder.addParameter("name=".concat(name));
	}
	
	public String pull(){
		
		try{
			
			logger.info("**Response**");
			logger.info(rBuilder.toString());
			
			rBuilder.doRequest();
			return rBuilder.getResponse();
		}
		catch(Exception e){
			
			logger.info("**Error**");
			logger.error(e.getMessage());
			
			e.printStackTrace();
			
			return null;
		}
	}
}
