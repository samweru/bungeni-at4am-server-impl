package org.bungeni.server.util;

import java.io.*;
import java.util.List;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.cxf.headers.Header;
import org.apache.cxf.helpers.CastUtils;

import org.apache.cxf.jaxws.context.WrappedMessageContext;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Util {
	
	static Logger logger = LoggerFactory.getLogger(Util.class);

	public static String nodeToString(Node node) {
    	
		StringWriter sw = new StringWriter();
		try {
			
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			t.transform(new DOMSource(node), new StreamResult(sw));
		} 
		catch (TransformerException e) {
			
			logger.info("**nodeToString Transformer Exception");
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		
		return sw.toString();
    }
	
	public static NodeList getNodeList(String path, String xml) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException{
		
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));
		doc.getDocumentElement().normalize();

		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList nodeList =  (NodeList) xpath.evaluate(path,doc,XPathConstants.NODESET);
		
		return nodeList;
	}

    public static String getWebInfFolderPath(){

        String firstPath = "web/WEB-INF";
        String secondPath = "WEB-INF";
        String webXmlFilename = "web.xml";
        String filePath=System.getProperty("user.dir");

        String tryPath = filePath.concat("/".concat(firstPath));
        File f = new File(tryPath.concat("/".concat(webXmlFilename)));
        if(f.exists())
            return tryPath;
        else
            return filePath.concat("/".concat(secondPath));
    }

    @Deprecated
	private static String tryPath(String[] paths){
		
		int i = 0;
		while(i<paths.length){
			
			String filePath=System.getProperty("user.dir").concat(paths[i]);
			
			logger.info("**checking path: ".concat(filePath));
			
			File f = new File(filePath);
			if(f.exists())
				return filePath;
			else
				logger.info("***not path: ".concat(filePath));
			i++;
		}
		
		return null;
	}
	
	public static Properties getProperties(String filePath){
		
		Properties properties = new Properties();
		InputStream is;
		
		try {
			
			is = new FileInputStream(filePath);
			properties.load(is);
			is.close();
			
			return properties;
		} catch (FileNotFoundException e) {
			
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static Properties getRestProperties(){
		
		logger.info("**rest properties");
        String filePath = Util.getWebInfFolderPath().concat("/rest.properties");
		Properties properties = getProperties(filePath);
		properties.list(System.out);
		
		return properties;
	}
	
	public static Properties getRestAuthProperties(){
		
		logger.info("**rest authentication properties");
        String filePath = Util.getWebInfFolderPath().concat("/rest.auth.properties");
		Properties properties = getProperties(filePath);
		return properties;
	}

    private static List<Header> getHeaders(WebServiceContext context) {
        MessageContext messageContext = context.getMessageContext();
        if (messageContext == null || !(messageContext instanceof WrappedMessageContext)) {
            return null;
        }

        Message message = ((WrappedMessageContext) messageContext).getWrappedMessage();
        List<Header> headers = CastUtils.cast((List<?>) message.get(Header.HEADER_LIST));
        return headers;
    }

    public static String getHeader(WebServiceContext context, String name) {

        List<Header> headers = Util.getHeaders(context);
        if (headers != null) {
            for (Header header : headers) {

                if(header.getName().equals(name)){

                    return (String)header.getObject();
                }
            }
        }
        return null;
    }

    public static String getPropertyAsString(Properties prop) {

        StringWriter writer = new StringWriter();
        prop.list(new PrintWriter(writer));
        return writer.getBuffer().toString();
    }

    public static boolean writeToFile(String filename, String content){

        Writer writer = null;

        try {

            File file = new File(filename);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    logger.error("**writeToFile: "+e.getMessage());
                }
            }
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(content);

            return true;
        } catch (FileNotFoundException e) {
            logger.error("**writeToFile: "+e.getMessage());
        } catch (IOException e) {
            logger.error("**writeToFile: "+e.getMessage());
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                logger.error("**writeToFile: "+e.getMessage());
            }
        }

        return false;
    }
}
