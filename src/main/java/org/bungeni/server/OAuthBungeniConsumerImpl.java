package org.bungeni.server;

import org.bungeni.server.properties.domain.OAuthProperties;
import org.bungeni.server.properties.repository.OAuthPropertiesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OAuthBungeniConsumerImpl {

	private String authUrl;
	private String accessTokenUrl;

	private String clientId;
	private String secretKey;
	private String responseType;
	private String grantType;
	
	private String method;
	
	private final Logger logger = LoggerFactory.getLogger(OAuthBungeniConsumerImpl.class);
	
	public OAuthBungeniConsumerImpl(){
		
		getProperties();
	}
	
	private void getProperties(){
		
		secretKey = OAuthPropertiesRepository.getSecretKey();

        OAuthProperties oauthProps = new OAuthProperties(OAuthPropertiesRepository.getProperties());
        String host=oauthProps.getHost();
        authUrl = host.concat(oauthProps.getAuthPath());
        accessTokenUrl = host.concat(oauthProps.getAccessTokenPath());
        clientId = oauthProps.getClientId();
        responseType = oauthProps.getResponseType();
        grantType = oauthProps.getGrantType();
        method=oauthProps.getMethod();
	}

	public String getAuthorizationUrl(){

		String newUrl = authUrl.concat("?client_id=".concat(clientId))
				.concat("&client_secret=".concat(secretKey))
			    .concat("&response_type=".concat(responseType));

		return newUrl;
	}

	public String getAccessToken(String authCode){

		RequestBuilder rb = new RequestBuilder();
		rb.setServiceUrl(accessTokenUrl);
		rb.setMethod(method);
		rb.addParameter("client_id=".concat(clientId));
		rb.addParameter("grant_type=".concat(grantType));
		rb.addParameter("code=".concat(authCode));
        try {
            rb.doRealRequest();
        } catch (Exception e) {

            logger.error("**getAccessToken: "+e.getMessage());
        }

        logger.info("**Access Token");
		logger.info("**accessToken: "+rb.toString());
		logger.info("**accessToken: "+rb.getResponse());
		
		return rb.getResponse();
	}
}
