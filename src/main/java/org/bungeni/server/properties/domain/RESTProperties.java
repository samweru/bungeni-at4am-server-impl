package org.bungeni.server.properties.domain;

import org.bungeni.server.util.Util;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 5/7/13
 * Time: 3:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class RESTProperties {

    private String host;
    private final String method="POST";
    private String attachmentPath;
    private String attachmentListPath;
    private String amendmentListPath;
    private String amendmentSavePath;
    private String amendmentDeletePath;

    private String username;
    private String password;

    public RESTProperties(Properties properties){

        host = properties.getProperty("host");
        attachmentListPath = host.concat(properties.getProperty("amendment-list-path"));
        attachmentListPath = host.concat(properties.getProperty("attachment-list-path"));
        amendmentDeletePath=host.concat(properties.getProperty("amendment-delete-path"));
        amendmentSavePath = host.concat(properties.getProperty("amendment-save-path"));
        attachmentPath = host.concat(properties.getProperty("attachment-path"));
    }

    public RESTProperties(Properties properties, Properties authProperties){

        new RESTProperties(properties);

        username = authProperties.getProperty("username");
        password = authProperties.getProperty("password");
    }

    public void setHost(String host){

        this.host=host;
    }

    public String getHost(){

        return host;
    }

    public void setUsername(String username){

        this.username=username;
    }

    public String getUsername(){

        return username;
    }

    public void setPassword(String password){

        this.password=password;
    }

    public String getPassword(){

        return password;
    }

    public String getMethod(){

        return method;
    }

    public void setAttachmentPath(String attachmentPath){

        this.attachmentPath=attachmentPath;
    }

    public String getAttachmentPath(){

        return attachmentPath;
    }

    public void setAttachmentListPath(String attachmentListPath){

        this.attachmentListPath=attachmentListPath;
    }

    public String getAttachmentListPath(){

        return attachmentListPath;
    }

    public String getAmendmentListPath(){

        return amendmentListPath;
    }

    public void setAmendmentListPath(String amendmentListPath){

        this.amendmentListPath=amendmentListPath;
    }

    public String getAmendmentSavePath(){

        return amendmentSavePath;
    }

    public void setAmendmentSavePath(String amendmentSavePath){

        this.amendmentSavePath=amendmentSavePath;
    }

    public String getAmendmentDeletePath(){

        return amendmentDeletePath;
    }

    public void setAmendmentDeletePath(String amendmentDeletePath){

        this.amendmentDeletePath=amendmentDeletePath;
    }
}
