package org.bungeni.server.properties.domain;

import org.bungeni.server.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 5/7/13
 * Time: 1:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class OAuthProperties {

    private String host;
    private String authPath;
    private String accessTokenPath;
    private String clientId;
    private String responseType;
    private String grantType;
    private String method;

    public OAuthProperties(Properties properties){

        host=properties.getProperty("host");
        authPath=properties.getProperty("auth-path");
        accessTokenPath=properties.getProperty("access-token-path");
        clientId=properties.getProperty("client-id");
        responseType=properties.getProperty("response-type");
        grantType=properties.getProperty("grant-type");
        method=properties.getProperty("method");
    }

    public void setHost(String host){

        this.host=host;
    }

    public String getHost(){

        return host;
    }

    public void setAuthPath(String authPath){


        this.authPath=authPath;
    }

    public String getAuthPath(){

        return authPath;
    }

    public void setAccessTokenPath(String accessTokenPath){

        this.accessTokenPath=accessTokenPath;
    }

    public String getAccessTokenPath(){

        return accessTokenPath;
    }

    public void setClientId(String clientId){

        this.clientId=clientId;
    }

    public String getClientId(){

        return clientId;
    }

    public void setResponseType(String responseType){

        this.responseType=responseType;
    }

    public String getResponseType(){

        return responseType;
    }

    public void setGrantType(String grantType){

        this.grantType=grantType;
    }

    public String getGrantType(){

        return grantType;
    }

    public void setMethod(String method){

        this.method=method;
    }

    public String getMethod(){

        return method;
    }

    public String toString(){

        String props="host=".concat(host).concat("\n");
        props+="auth-path=".concat(authPath).concat("\n");
        props+="access-token-path=".concat(accessTokenPath).concat("\n");
        props+="client-id=".concat(clientId).concat("\n");
        props+="response-type=".concat(responseType).concat("\n");
        props+="grant-type=".concat(grantType).concat("\n");
        props+="method=".concat(method).concat("\n");

        return props;
    }
}
