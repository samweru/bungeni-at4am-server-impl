package org.bungeni.server.properties.domain;

import org.bungeni.server.util.Util;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 5/7/13
 * Time: 3:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class APIProperties {

    private String method;
    private String host;
    private String apiUrlWorkSpaceSections;
    private String apiUrlWorkSpaceInbox;
    private String apiUrlUserList;
    private String apiUrlCurrentUser;

    public APIProperties(Properties properties){

        host = properties.getProperty("host");
        apiUrlWorkSpaceSections = host.concat(properties.getProperty("api-path-workspace-sections"));
        apiUrlWorkSpaceInbox = host.concat(properties.getProperty("api-path-workspace-inbox"));
        apiUrlUserList = host.concat(properties.getProperty("api-path-user-list"));
        apiUrlCurrentUser = host.concat(properties.getProperty("api-path-current-user"));

        method = properties.getProperty("method");
    }

    public String getMethod(){

        return method;
    }

    public void setMethod(String method){

        this.method=method;
    }

    public void setHost(String host){

        this.host=host;
    }

    public String getHost(){

        return host;
    }

    public void setUrlWorkSpaceSections(String apiUrlWorkSpaceSections){

        this.apiUrlWorkSpaceSections=apiUrlWorkSpaceSections;
    }

    public String getUrlWorkSpaceSections(){

        return apiUrlWorkSpaceSections;
    }

    public void setUrlWorkspaceInbox(String apiUrlWorkSpaceInbox){

        this.apiUrlWorkSpaceInbox=apiUrlWorkSpaceInbox;
    }

    public String getUrlWorkspaceInbox(){

        return apiUrlWorkSpaceInbox;
    }

    public void setUrlUserList(String apiUrlUserList){

        this.apiUrlUserList=apiUrlUserList;
    }

    public String getUrlUserList(){

        return apiUrlUserList;
    }

    public void setUrlCurrentUser(String apiUrlCurrentUser){

        this.apiUrlCurrentUser=apiUrlCurrentUser;
    }

    public String getUrlCurrentUser(){

        return apiUrlCurrentUser;
    }
}
