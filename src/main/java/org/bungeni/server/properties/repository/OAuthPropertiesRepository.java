package org.bungeni.server.properties.repository;

import org.bungeni.server.properties.domain.OAuthProperties;
import org.bungeni.server.util.Util;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 5/7/13
 * Time: 2:30 PM
 * To change this template use File | Settings | File Templates.
 */
public class OAuthPropertiesRepository {

    private static final String filePath = Util.getWebInfFolderPath().concat("/oauth.properties");

    public static Properties getProperties(){

        return Util.getProperties(filePath);
    }

    public static boolean save(OAuthProperties oauthProps){

        return Util.writeToFile(filePath, oauthProps.toString());
    }

    public static String getSecretKey(){

        String fileOAuthPath = Util.getWebInfFolderPath().concat("/oauth.key.properties");
        Properties properties = Util.getProperties(fileOAuthPath);

        return properties.getProperty("secret-key");
    }
}
