package org.bungeni.server.properties.repository;

import org.bungeni.server.util.Util;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 5/7/13
 * Time: 3:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class APIPropertiesRepository {

    public static Properties getProperties(){

        String filePath = Util.getWebInfFolderPath().concat("/api.properties");
        Properties properties = Util.getProperties(filePath);

        return properties;
    }
}
