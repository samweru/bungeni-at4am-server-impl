package org.bungeni.server.properties.repository;

import org.bungeni.server.util.Util;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 5/7/13
 * Time: 3:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class RESTPropertiesRepository {

    public static Properties getProperties(){

        String filePath = Util.getWebInfFolderPath().concat("/rest.properties");
        Properties properties = Util.getProperties(filePath);
        return properties;
    }

    public static Properties getAuthProperties(){

        String filePath = Util.getWebInfFolderPath().concat("/rest.auth.properties");
        Properties properties = Util.getProperties(filePath);
        return properties;
    }
}
