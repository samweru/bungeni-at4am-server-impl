package org.bungeni.server.converter;

import com.inspiresoftware.lib.dto.geda.adapter.BeanFactory;
import com.inspiresoftware.lib.dto.geda.adapter.ValueConverter;

import org.bungeni.server.domain.AmendmentContainer;
import org.bungeni.server.domain.Document;
import org.bungeni.server.repository.DocumentRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * Date: 13/03/13 14:35
 *
 * @author <a href="philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
@Component("documentIDConvertor")
@Lazy
public class DocumentIDConvertor implements ValueConverter {

    DocumentRepository documentRepository;

    @Override
    public Object convertToDto(Object object, BeanFactory beanFactory) {

        if (object == null) return null;

        final AmendmentContainer amendmentContainer = (AmendmentContainer) object;
        return amendmentContainer.getDocument() != null ? amendmentContainer.getDocument().getDocumentID() : null;
    }

    @Override
    public Object convertToEntity(Object object, Object oldEntity, BeanFactory beanFactory) {

        if (object == null) return null;

        final Document document = DocumentRepository.findDocumentById((String) object);
        if (oldEntity instanceof AmendmentContainer) {
            ((AmendmentContainer)oldEntity).setDocument(document);
        }
        return document;
    }
}