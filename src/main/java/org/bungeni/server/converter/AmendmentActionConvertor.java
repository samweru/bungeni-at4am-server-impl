package org.bungeni.server.converter;

import com.inspiresoftware.lib.dto.geda.adapter.BeanFactory;
import com.inspiresoftware.lib.dto.geda.adapter.ValueConverter;
import org.bungeni.server.domain.AmendmentContainer;
import org.bungeni.server.dto.AmendmentAction;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * Date: 13/03/13 14:35
 *
 * @author <a href="philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
@Component("amendmentActionConvertor")
@Lazy
public class AmendmentActionConvertor implements ValueConverter {

    @Override
    public Object convertToDto(Object object, BeanFactory beanFactory) {

        if (object == null) return null;

        final AmendmentContainer amendmentContainer = (AmendmentContainer) object;
        if (amendmentContainer.getAmendmentAction() != null) {
            return AmendmentAction.valueOf(amendmentContainer.getAmendmentAction().name());
        }
        return null;
    }

    @Override
    public Object convertToEntity(Object object, Object oldEntity, BeanFactory beanFactory) {

        if (object == null) return null;
        AmendmentAction dto = (AmendmentAction) object;

        if (oldEntity instanceof AmendmentContainer) {
            final org.bungeni.server.domain.AmendmentAction amendmentAction = org.bungeni.server.domain.AmendmentAction.valueOf(dto.name());
            ((AmendmentContainer)oldEntity).setAmendmentAction(amendmentAction);
            return amendmentAction;
        }
        return null;
    }
}
