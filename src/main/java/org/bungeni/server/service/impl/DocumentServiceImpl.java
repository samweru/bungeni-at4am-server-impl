package org.bungeni.server.service.impl;

import com.google.common.io.Files;
import org.apache.cxf.annotations.GZIP;
import org.bungeni.server.domain.Document;
import org.bungeni.server.repository.DocumentRepository;
import org.bungeni.server.service.api.DocumentService;
import org.bungeni.server.dto.DocumentDTO;
//import org.nsesa.server.service.api.DocumentService;


import com.inspiresoftware.lib.dto.geda.assembler.Assembler;
import com.inspiresoftware.lib.dto.geda.assembler.DTOAssembler;
import com.inspiresoftware.lib.dto.geda.assembler.dsl.impl.DefaultDSLRegistry;
import org.springframework.core.io.Resource;
//import com.inspiresoftware.lib.dto.geda.assembler.dsl.impl.DefaultDSLRegistry;
//import org.nsesa.server.repository.DocumentRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
//import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Date: 11/03/13 15:52
 *
 * @author <a href="philip.luppens@gmail.com">Philip Luppens</a>
 * @version $Id$
 */
@WebService(endpointInterface = "org.bungeni.server.service.api.DocumentService", serviceName = "DocumentService")
@GZIP
@Path("/document")
public class DocumentServiceImpl implements DocumentService {

    final Assembler documentAssembler = DTOAssembler.newAssembler(DocumentDTO.class, Document.class);

    @GET
    @Path("/id/{documentID}")
    @Produces({MediaType.APPLICATION_XML, MediaType.TEXT_XML, MediaType.APPLICATION_JSON})
    @Override
    //public Response getDocument(@PathParam("documentID") String documentID) {
    public DocumentDTO getDocument(@PathParam("documentID") String documentID) {
        
    	Document document = DocumentRepository.findDocumentById(documentID);
    	DocumentDTO documentDTO = new DocumentDTO();
        documentAssembler.assembleDto(documentDTO, document, getConvertors() , new DefaultDSLRegistry());
        //return Response.ok(document).type("text/xml").build();
        return documentDTO;
    }

    @Override
    public DocumentDTO saveDocument(@WebParam(name = "documentDTO") DocumentDTO documentDTO) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<DocumentDTO> list(@WebParam(name = "offset") int i, @WebParam(name = "rows") int i2) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<DocumentDTO> getAvailableTranslations(@WebParam(name = "documentID") String s) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public List<DocumentDTO> getRelatedDocuments(@WebParam(name = "documentID") String s) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @GET
    @Path("/content/{documentID}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Override
    public String getDocumentContent(@PathParam("documentID") String documentID) {

        //String documentContent = DocumentRepository.getAttachmentById(documentID);
        //return documentContent;

        return null;
    }

    @POST
    @Path("/save")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public void save(DocumentDTO documentDTO) {
    	
    }

    @GET
    @Path("/search")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN})
    @Override
    public Response findAll(@QueryParam("text") String text,
    		@QueryParam("page") String page,
    		@QueryParam("recordsPerPage") String recordsPerPage) {
    	
    	String xmlDocumentList=DocumentRepository.getPagedAttachmentList(text, page, recordsPerPage);
    	return Response.ok(xmlDocumentList).type("text/xml").build();
    }

    //private Document fromDocumentDTO(DocumentDTO documentDTO) {
    //    Document document = new Document();
    //    document.setDocumentID(documentDTO.getDocumentID());
    //    document.setName(documentDTO.getName());
    //    document.setLanguageIso(documentDTO.getLanguageIso());
    //    document.setAmendable(documentDTO.isAmendable());
    //    //document.setDeadline(documentDTO.getDeadline());
    //    return document;
    // }

    private Map<String, Object> getConvertors() {
        return new HashMap<String, Object>();
    }

	//@Override
	//public List<DocumentDTO> findAll() {
		// TODO Auto-generated method stub
		//return null;
	//}
}