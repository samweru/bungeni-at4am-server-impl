package org.bungeni.server.service.impl;

import org.apache.cxf.annotations.GZIP;
import org.bungeni.server.properties.domain.OAuthProperties;
import org.bungeni.server.properties.repository.OAuthPropertiesRepository;
import org.bungeni.server.util.Util;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 4/22/13
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */

@GZIP
@Path("/properties")
public class PropertiesServiceImpl {

    private final Logger logger = LoggerFactory.getLogger(PropertiesServiceImpl.class);

    @POST
    @Path("/write/oauth")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Consumes("application/x-www-form-urlencoded")
    public Response write(MultivaluedMap<String, String> params) {


        OAuthProperties oauthProps = new OAuthProperties(OAuthPropertiesRepository.getProperties());
        oauthProps.setHost(params.getFirst("host"));
        oauthProps.setClientId(params.getFirst("client-id"));
        oauthProps.setResponseType(params.getFirst("response-type"));
        oauthProps.setGrantType(params.getFirst("grant-type"));
        oauthProps.setMethod(params.getFirst("method"));

        JSONObject jso = new JSONObject();

        try {
            if(OAuthPropertiesRepository.save(oauthProps))
                jso.put("success","true");
            else
                jso.put("success","false");
        } catch (JSONException e) {

            logger.error(e.getMessage());
        }

        return Response.ok(jso.toString()).type("application/json").build();
    }

    @GET
    @Path("/oauth")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response oauthRead(@Context HttpServletResponse response, @Context HttpServletRequest request) throws JSONException {

        OAuthProperties oauthProps = new OAuthProperties(OAuthPropertiesRepository.getProperties());

        JSONObject j = new JSONObject();
        j.put("host",oauthProps.getHost());
        j.put("authPath",oauthProps.getAuthPath());
        j.put("accessTokenPath",oauthProps.getAccessTokenPath());
        j.put("clientId",oauthProps.getClientId());
        j.put("responseType",oauthProps.getResponseType());
        j.put("grantType",oauthProps.getGrantType());
        j.put("method",oauthProps.getMethod());

        return Response.ok(j.toString()).type("application/json").build();
    }

    @GET
    @Path("/rest")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response restRead(@Context HttpServletResponse response, @Context HttpServletRequest request) throws JSONException {

        Properties p = Util.getRestAuthProperties();

        JSONObject j = new JSONObject();
        j.put("username",p.getProperty("username"));

        return Response.ok(j.toString()).type("application/json").build();
    }
}
