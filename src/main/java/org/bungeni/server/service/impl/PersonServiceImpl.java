package org.bungeni.server.service.impl;

import com.inspiresoftware.lib.dto.geda.assembler.Assembler;
import com.inspiresoftware.lib.dto.geda.assembler.DTOAssembler;
import com.inspiresoftware.lib.dto.geda.assembler.dsl.impl.DefaultDSLRegistry;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.bungeni.server.RequestBuilder;
import org.bungeni.server.domain.Person;
import org.bungeni.server.repository.PersonRepository;
import org.bungeni.server.dto.PersonDTO;
import org.bungeni.server.service.api.PersonService;
import org.bungeni.server.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientFactory;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.io.*;
import java.util.*;

@WebService(endpointInterface = "org.bungeni.server.service.api.PersonService", serviceName = "PersonService")
@GZIP
@Path("/person")
public class PersonServiceImpl implements PersonService{

    @Resource
    WebServiceContext wsContext;

    private final Assembler personAssembler = DTOAssembler.newAssembler(PersonDTO.class, Person.class);
    private final Logger logger = LoggerFactory.getLogger(PersonServiceImpl.class);

    @GET
    @Path("/test")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    //@Override
    public Response test(){

        WebClient wc = WebClient.create("http://127.0.0.1:9991/ws/rest");
        wc.path("oauth/test/dump/cache");
        Response r = wc.get();


        String concated = "Nothing";
        try {

            concated = IOUtils.toString((InputStream)r.getEntity());
            logger.info("test-entity: "+r.getEntity());
            logger.info("test-string: "+concated);
            logger.info("test-reflect: "+ReflectionToStringBuilder.toString(concated));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        //return Response.ok(r).type("application/json").build();
        return Response.ok(concated).type("application/json").build();
    }

    @GET
    @Path("/list")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Override
    public List<PersonDTO> getUserList() {

        //List<Person> persons = new ArrayList<Person>();
        //persons.add(new Person("1", "mep1", "mep1", "MEP"));
        //persons.add(new Person("2", "mep2", "mep2", "MEP"));
        //persons.add(new Person("3", "mep3", "mep3", "MEP"));

        //List<PersonDTO> personDTOs = new ArrayList<PersonDTO>();
        //personAssembler.assembleDto(personDTOs, persons, getConvertors(), new DefaultDSLRegistry());
        //return personDTOs;

        return null;
    }

    @GET
    @Path("/current")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public PersonDTO getCurrentUser(){

        Message message = PhaseInterceptorChain.getCurrentMessage();
        HttpServletResponse response = (HttpServletResponse)message.get(AbstractHTTPDestination.HTTP_RESPONSE);
        HttpServletRequest request = (HttpServletRequest)message.get(AbstractHTTPDestination.HTTP_REQUEST);
        String code = (String) request.getSession().getAttribute("code");

        logger.info("**getCurrentUser-codeX: "+code);


        //String accessToken = Util.getHeader(context,"AccessToken");
        //logger.info("getCurrentUser-accessTokenX"+accessToken);
        //HttpSession  session = request.getSession(true);

        //logger.info("**getCurrentUser-code: " + session.getAttribute("code"));
        //logger.info("**getCurrentUser-accessToken: " + session.getAttribute("accessToken"));

        // Get a session property "counter" from context
        //if (session == null)
            //logger.info("**getCurrentUser: No session in WebServiceContext");

        //session.setAttribute("testSession","test-session");

        //CrossServiceImpl crossService = new CrossServiceImpl();
        //String userCredentials = crossService.getCurrentCredentials();

        //logger.info("**getCurrentUser-userCredentials: "+userCredentials);

        Person person = new Person();
        person.setPersonID(UUID.randomUUID().toString());
        person.setLastName("Weru");
        person.setName("Sam");
        person.setUsername("samweru");
        person.setID(Long.parseLong("273948723"));

        PersonDTO personDTO = new PersonDTO();
        personAssembler.assembleDto(personDTO, person, getConvertors(), new DefaultDSLRegistry());
        return personDTO;
    }


    @GET
    @Path("/id/{personID}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Override
    public PersonDTO getPerson(@PathParam("personID") String personID) {

        Person person = PersonRepository.findByPersonID(personID);
        if (person == null) {
            person = new Person(personID, "guest-" + UUID.randomUUID().toString(), "Guest", "GUEST");
            //PersonRepository.save(person);
        }
        PersonDTO personDTO = new PersonDTO();
        personAssembler.assembleDto(personDTO, person, getConvertors(), new DefaultDSLRegistry());
        return personDTO;
    }

    @GET
    @Path("/username/{username}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Override
    public PersonDTO getPersonByUsername(@PathParam("username") String username) {
        Person person = PersonRepository.findByUsername(username);
        if (person == null) {
            person = new Person(UUID.randomUUID().toString(), username, username, "GUEST");
            //PersonRepository.save(person);
        }
        PersonDTO personDTO = new PersonDTO();
        personAssembler.assembleDto(personDTO, person, getConvertors(), new DefaultDSLRegistry());
        return personDTO;
    }

    @POST
    @Path("/create")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Override
    public void save(PersonDTO personDTO) {
        //Person person = new Person();
        //personAssembler.assembleEntity(personDTO, person, getConvertors(), new DefaultDSLRegistry());
        //personRepository.save(person);
    }

    private Map<String, Object> getConvertors() {
        return new HashMap<String, Object>();
    }
}
