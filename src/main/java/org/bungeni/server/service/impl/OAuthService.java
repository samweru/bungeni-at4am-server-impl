package org.bungeni.server.service.impl;


import javax.annotation.Resource;
import javax.jws.WebResult;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.xml.ws.WebServiceContext;

/**
 * Created with IntelliJ IDEA.
 * User: undesa
 * Date: 5/3/13
 * Time: 3:54 PM
 * To change this template use File | Settings | File Templates.
 */

@javax.jws.WebService
public interface OAuthService {

    public Response testSession(HttpServletResponse response, HttpServletRequest request);
    public Response testCheckSession(HttpServletResponse response, HttpServletRequest request);
    public Response testCache(HttpServletResponse response, HttpServletRequest request);
    public Response testCheckCache(HttpServletResponse response, HttpServletRequest request);
    public Response testSetCache(HttpServletResponse response, HttpServletRequest request);
    public Response testDumpCache(HttpServletResponse response, HttpServletRequest request);
    public Response authorized(HttpServletResponse response, HttpServletRequest request);
    public Response authorize(HttpServletResponse response, HttpServletRequest request);
    public Response accessToken(HttpServletResponse response, HttpServletRequest request);
    public Response authCheck(HttpServletResponse response, HttpServletRequest request);
    public Response authSave(HttpServletResponse response, HttpServletRequest request, MultivaluedMap<String, String> params);
    public Response propertiesLogin(HttpServletResponse response, HttpServletRequest request, MultivaluedMap<String, String> params);

}
