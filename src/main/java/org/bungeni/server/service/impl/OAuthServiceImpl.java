package org.bungeni.server.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.*;

import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.apache.cxf.Bus;
import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.bus.CXFBusFactory;
import org.apache.cxf.endpoint.*;
import org.apache.cxf.frontend.ClientFactoryBean;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.headers.Header;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.ws.rm.Source;
import org.bungeni.server.BungeniAPIImpl;
import org.bungeni.server.OAuthBungeniConsumerImpl;
import org.bungeni.server.service.api.PersonService;
import org.bungeni.server.util.Cache;
import org.bungeni.server.util.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@GZIP
@Path("/oauth")
public class OAuthServiceImpl implements OAuthService{
	
	private OAuthBungeniConsumerImpl oauthC = new OAuthBungeniConsumerImpl();
    private BungeniAPIImpl api = new BungeniAPIImpl();

    private final Logger logger = LoggerFactory.getLogger(OAuthServiceImpl.class);

    @Resource
    WebServiceContext wsContext;

    private Cache<String, String> c = new Cache<String, String>(10000);

    @GET
    @Path("/editor/auth/check")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response authCheck(@Context HttpServletResponse response, @Context HttpServletRequest request){

        String fileOAuthPath = Util.getWebInfFolderPath().concat("/editor.pass.properties");
        Properties properties = Util.getProperties(fileOAuthPath);

        String password = properties.getProperty("password");
        JSONObject jso = new JSONObject();
        try {
            if(password==null)
                jso.put("success","false");
            else
                jso.put("success","true");
        } catch (JSONException e) {

            logger.info("authCheck: "+e.getMessage());
        }

        request.getSession().setAttribute("allowSave","False");
        String _csrfToken = (String) request.getSession().getAttribute("_csrf");

        if(_csrfToken == null){

            _csrfToken = UUID.randomUUID().toString();
            request.getSession().setAttribute("_csrf",_csrfToken);
        }

        return Response.ok(jso.toString()).header("_csrf", _csrfToken).type("application/json").build();
    }

    @POST
    @Path("/editor/properties/login")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response propertiesLogin(@Context HttpServletResponse response, @Context HttpServletRequest request, MultivaluedMap<String, String> params){

        String fileOAuthPath = Util.getWebInfFolderPath().concat("/editor.pass.properties");
        Properties properties = Util.getProperties(fileOAuthPath);

        String propUsername = properties.getProperty("username");
        String propPassword = properties.getProperty("password");
        String paramUsername = params.getFirst("username");
        String paramPassword = params.getFirst("password");

        logger.info("propertiesLogin-prop-username: "+propUsername);
        logger.info("propertiesLogin-prop-password: "+propPassword);
        logger.info("propertiesLogin-param-username: "+paramUsername);
        logger.info("propertiesLogin-param-password: "+paramPassword);

        JSONObject jso = new JSONObject();
        try {
            if(propUsername.equals(paramUsername) && paramPassword.equals(propPassword)){

                request.getSession().setAttribute("allowSave","True");
                jso.put("success", "true");
            }
            else
                jso.put("success","false");
        } catch (JSONException e) {

            logger.info("authCheck: "+e.getMessage());
        }

        return Response.ok(jso.toString()).header("_csrf", request.getSession().getAttribute("_csrf")).type("application/json").build();
    }

    @POST
    @Path("/editor/auth/save")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response authSave(@Context HttpServletResponse response, @Context HttpServletRequest request, MultivaluedMap<String, String> params){

        Object o = request.getSession().getAttribute("allowSave").toString();
        if("False"==o.toString())
            try {
                response.sendRedirect("/");
            } catch (IOException e) {
                logger.error("authSave: "+e.getMessage());
            }

        boolean save = false;
        String properties = "";

        String fileEditorPass = Util.getWebInfFolderPath().concat("/editor.pass.properties");
        String username = params.getFirst("username");
        String password = params.getFirst("password");
        String confirmPassword = params.getFirst("confirm-password");

        if(!username.isEmpty() && !password.isEmpty())
            if(confirmPassword.equals(password)){

                save=true;
                properties+="username="+username+"\n";
                properties+="password="+password;
            };

        logger.info("authSave-properties: "+properties);
        logger.info("authSave-fileEditorPass: "+fileEditorPass);
        logger.info("authSave-save: "+save);

        JSONObject jso = new JSONObject();
        try {
            if(Util.writeToFile(fileEditorPass, properties) && save==true)
                try {
                    response.sendRedirect("/");
                } catch (IOException e) {
                    logger.error("authSave: "+e.getMessage());
                }
            else
                jso.put("success", "false");
        } catch (JSONException e) {

            logger.info("authSave: "+e.getMessage());
        }

        request.getSession().setAttribute("allowSave", "False");
        return Response.ok(jso.toString()).header("_csrf", request.getSession().getAttribute("_csrf")).type("application/json").build();
    }

    @GET
    @Path("/authorized")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response authorized(@Context HttpServletResponse response, @Context HttpServletRequest request){

        JSONObject jso = new JSONObject();
        String accessToken = (String) request.getSession().getAttribute("accessToken");
        logger.info("authorized-accessToken: "+accessToken);

        try {
            if(accessToken==null)
                jso.put("authorized","false");
            else
                jso.put("authorized","true");
        } catch (JSONException e) {

            logger.info("authorized: "+e.getMessage());
        }


        //return Response.ok(jso.toString()).type("application/json").build();
        return Response.ok(jso.toString()).header("_csrf", request.getSession().getAttribute("_csrf")).type("application/json").build();
    }

	@GET
    @Path("/authorize")
    public Response authorize(@Context HttpServletResponse response, @Context HttpServletRequest request){
		
		HttpSession session = request.getSession();
		String code = (String) session.getAttribute("code");

        logger.info("**authorize-code: "+(String)session.getAttribute("code"));
        logger.info("**authorize-accessToken: "+(String)session.getAttribute("accessToken"));
        logger.info("**authorize-testSession: " + (String)session.getAttribute("testSession"));
		
		try{
			
			if(!code.isEmpty());
                response.sendRedirect("/index.html");
		}
		catch(Exception e){

            try {

                response.sendRedirect(oauthC.getAuthorizationUrl());
            } catch (IOException e1) {

                logger.error("authorize: "+e1.getMessage());
            }
        }
		return null;
	}

//  {"mimetype": "text/xml",
// "status": {"name": "status", "value": "draft", "displayAs": "draft"},
// "attachment_id": 145,
// "description": "Kenya Communications Bill",
// "language": {"name": "language", "value": "en", "displayAs": "en"},
// "title": "Kenya Communications Bill",
// "head_id": 154,
// "status_date": "2013-04-16T17:32:05.874149",
// "type": {"name": "type", "value": "main-xml", "displayAs": "Main Document (AkomaNtoso xml)"},
// "name": "Bill_Kenya_2006-02-10.xml"}


	@GET
	@Path("/access/token")
    @Override
	public Response accessToken(@Context HttpServletResponse response, @Context HttpServletRequest request){

		HttpSession session = request.getSession();
		String authCode = request.getParameter("code");
		session.setAttribute("authCode", authCode);
        logger.info("**accessToken-authCode: "+(String)session.getAttribute("authCode"));


		String accessTokenJsStr = oauthC.getAccessToken(authCode);
		
		String accessToken = "";
		try {

			JSONObject jsAccessToken = new JSONObject(accessTokenJsStr);
			accessToken = (String) jsAccessToken.get("access_token");

            JSONObject jo = new JSONObject();

            try {

                String _csrf = (String) request.getSession().getAttribute("_csrf");
                jo.put("accessToken",accessToken);
                jo.put("csrfToken", _csrf);

                c.put(_csrf, jo.toString());

            } catch (JSONException e) {

                logger.error("**accessToken: " + e.getMessage());
            }

            session.setAttribute("accessToken", accessToken);
            logger.info("**accessToken-accessToken: "+(String)session.getAttribute("accessToken"));

            String currentUser = api.getCurrentUser(accessToken);
            logger.info("**currentUser: "+currentUser);

            response.sendRedirect("/index.html");

        } catch (IOException ioe) {

             logger.error("**accessToken: "+ioe.getMessage());

        } catch (JSONException e) {

            logger.error("**accessToken: "+e.getMessage());
		}
		
//		api.getWorkspaceSections(accessToken);
//      api.getUsersList(accessToken);

        try {

            JSONObject jsFilteredInbox = new JSONObject(api.getWorkspaceFilteredInboxByStatus(accessToken, "submitted"));
            JSONArray jsArrFilteredInbox = (JSONArray) jsFilteredInbox.get("nodes");
            /**/
            logger.info("**testString: "+jsFilteredInbox.get("nodes").toString());
            /**/

            for(int n = 0; n < jsArrFilteredInbox.length(); n++){

                JSONObject node = jsArrFilteredInbox.getJSONObject(n);
                String objectId = (String) node.get("object_id");
                logger.info("**accessToken-submitted-node"+node.toString());
                logger.info("**accessToken-ObjectID: "+objectId);

                JSONObject jsFilteredInboxAttachments = new JSONObject(api.getInboxItemAttachmentsDetails(accessToken, objectId));
                /**/
                logger.info("**testString-nested: "+jsFilteredInboxAttachments.get("nodes").toString());
                /**/

                JSONArray jsArrFilteredInboxAttachments = (JSONArray) jsFilteredInboxAttachments.get("nodes");
                logger.info("**accessToken-testNodes-length: "+jsArrFilteredInboxAttachments.length());

                if(jsArrFilteredInboxAttachments.length()>0)
                    for(int m = 0; m < jsArrFilteredInboxAttachments.length(); m++){

                        JSONObject nod = jsArrFilteredInboxAttachments.getJSONObject(m);
                        String objectId2 = (String) nod.get("object_id");
                        logger.info("**accessToken-ObjectID2: "+objectId2);
                        //api.getInboxItemDetails(accessToken, objectId) ;

                        String filteredInboxAttachmentDetails =  api.getInboxItemAttachmentDetails(accessToken, objectId, objectId2);
                        logger.info("**accessToken-attachmentDetails: "+filteredInboxAttachmentDetails);

                        logger.info("**accessToken-node: "+nod);
                    }

            }

        } catch (JSONException e) {

            logger.error("accessToken-: "+e.getMessage());
        }

        return null;
	}


    @GET
    @Path("/test/dump/cache")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response testDumpCache(@Context HttpServletResponse response, @Context HttpServletRequest request){
        //public Response testSession(){

        //HttpServletRequest request =(HttpServletRequest)wsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
        //request.getSession().setAttribute("testSessionVariable","sess982734971293478979");
        //String dump = "";
        JSONArray ja = new JSONArray();
        for (Map.Entry<String, String> e : c.cache.entrySet()) {
            logger.info("**testDumpCache: " + e.getKey() + " : " + e.getValue());
            ja.put(e.getValue());
            //dump += e.getKey()+"=="+e.getValue();
        };
        //return Response.ok("test").type("application/json").build();
        //if(dump.isEmpty())
          //  dump.concat("Nothing");

        return Response.ok(ja.toString()).type("application/json").build();
    }

    @GET
    @Path("/test/set/cache")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response testSetCache(@Context HttpServletResponse response, @Context HttpServletRequest request){
        //public Response testSession(){

        //HttpServletRequest request =(HttpServletRequest)wsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
        //request.getSession().setAttribute("testSessionVariable","sess982734971293478979");

        UUID uuid = UUID.randomUUID();
        JSONObject jo = new JSONObject();
        try {
            jo.put("UUID",uuid.toString());
            jo.put("accessToken","");
            jo.put("csrfToken","");
            jo.put("cookie","");
        } catch (JSONException e) {
            logger.info("**testSetCache: "+e.getMessage());
        }
        c.put(uuid.toString(),jo.toString());
        request.getSession().setAttribute("currentUUID", uuid.toString());
        request.getSession().setAttribute(uuid.toString(),jo.toString());
        //return Response.ok("test").type("application/json").build();
        return Response.ok("testCache").type("application/json").build();
    }

    @GET
    @Path("/test/check/cache")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response testCheckCache(@Context HttpServletResponse response, @Context HttpServletRequest request){
        //public Response checkSession(){

        //HttpServletRequest request =(HttpServletRequest)wsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);

        String t = (String) request.getSession().getAttribute("currentUUID");
        //return Response.ok(t).type("application/json").build();
        String d = c.get(t);
        return Response.ok(d).type("application/json").build();
    }

    @GET
    @Path("/test/cache")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    //@Override
    public Response testCache(@Context HttpServletResponse response, @Context HttpServletRequest request){

        //HttpServletRequest request =(HttpServletRequest)wsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
        //HttpSession s = request.getSession();
        //String r = s.getAttribute("testSessionVariable").toString();


        String d = c.get("testCache");
        return Response.ok(d).type("application/json").build();
//        return Response.ok(r).type("application/json").build();


        //return Response.ok("my-test").type("application/json").build();
    }

    @GET
    @Path("/test/check/session")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    //@Override
    public Response testCheckSession(@Context HttpServletResponse response, @Context HttpServletRequest request){
    //public Response checkSession(){

        //HttpServletRequest request =(HttpServletRequest)wsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);

        String t = (String) request.getSession().getAttribute("testSessionVariable");
        return Response.ok(t).type("application/json").build();


    }

    @GET
    @Path("/test/session")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    //@Override
    public Response testSession(@Context HttpServletResponse response, @Context HttpServletRequest request){

        //HttpServletRequest request =(HttpServletRequest)wsContext.getMessageContext().get(MessageContext.SERVLET_REQUEST);
        HttpSession s = request.getSession();
        String r = s.getAttribute("testSessionVariable").toString();


        return Response.ok(r).type("application/json").build();


        //return Response.ok("my-test").type("application/json").build();
    }



    @GET
    @Path("/whoami")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    public Response whoAmI(@Context HttpServletResponse response, @Context HttpServletRequest request){


//        String secretKey = request.getParameter("secretKey");
//        logger.info("**whoAmI-secretKey: "+secretKey);
//        HttpSession session = request.getSession();
//        String accessToken = (String) session.getAttribute("accessToken");
//        logger.info("**whoAmI-accessToken: "+accessToken);
//        String key = Util.getOAuthSecretKey();
//        logger.info("**whoAmI-key: "+key);
//
//        String currentUser = "";
//        if(secretKey.equals(key))
//            currentUser = api.getCurrentUser(accessToken);
//
//        logger.info("**whoAmI-currentUser: "+currentUser);
//
//        return Response.ok(currentUser).type("application/json").build();

        //OAuthServiceImpl o = JAXRSClientFactory.create("http://localhost:9991/ws/rest/oauth", OAuthServiceImpl.class);
// (1) remote GET call to http://bookstore.com/bookstore
        //Response r =  o.test();
        //logger.info("**whoAmI: "+r.getEntity().toString());
// (2) no remote call
        //BookResource subresource = store.getBookSubresource(1);
// {3} remote GET call to http://bookstore.com/bookstore/1
        //Book b = subresource.getBook()

        return Response.ok("").type("application/json").build();
    }
}
