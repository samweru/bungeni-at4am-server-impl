package org.bungeni.server.service.impl;

import java.io.IOException;
import java.util.*;

import com.inspiresoftware.lib.dto.geda.adapter.ValueConverter;
import com.inspiresoftware.lib.dto.geda.assembler.Assembler;
import com.inspiresoftware.lib.dto.geda.assembler.DTOAssembler;
import com.inspiresoftware.lib.dto.geda.assembler.dsl.impl.DefaultDSLRegistry;
import org.apache.cxf.annotations.GZIP;
import org.bungeni.server.domain.AmendableWidgetReference;
import org.bungeni.server.domain.AmendmentContainer;
import org.bungeni.server.domain.Document;
import org.bungeni.server.domain.Person;
import org.bungeni.server.repository.AmendmentContainerRepository;
import org.bungeni.server.repository.DocumentRepository;
import org.bungeni.server.repository.PersonRepository;
import org.bungeni.server.util.Util;
import org.bungeni.server.dto.AmendmentAction;
import org.bungeni.server.dto.AmendmentContainerDTO;
import org.bungeni.server.service.api.AmendmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.jws.WebService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;


@WebService(endpointInterface = "org.bungeni.server.service.api.AmendmentService", serviceName = "AmendmentService")
@GZIP
@Path("/amendment")
public class AmendmentServiceImpl implements AmendmentService{

    private final Assembler amendmentContainerAssembler = DTOAssembler.newAssembler(AmendmentContainerDTO.class, AmendmentContainer.class);
    private final Logger logger = LoggerFactory.getLogger(AmendmentServiceImpl.class);

    @Autowired
    ValueConverter documentIDConvertor;

    @Autowired
    ValueConverter personIDConvertor;

    @Autowired
    ValueConverter amendmentActionConvertor;

    @Autowired
    ValueConverter amendableWidgetReferenceConvertor;

    @POST
    @Path("/delete/{amendmentContainerID}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Override
    public void delete(final @PathParam("amendmentContainerID") String amendmentContainerID) {
//        final AmendmentContainer container = AmendmentContainerRepository.findByAmendmentContainerID(amendmentContainerID);
//        if (container != null) {
            logger.info("**AmendmentContainerID: "+amendmentContainerID);
            AmendmentContainerRepository.delete(amendmentContainerID.concat("-am.xml"));
//        }
    }

	@Override
	public AmendmentContainerDTO getAmendmentContainer(String arg0) {
        logger.info("**********************************getAmendmentContainer");
		return null;
	}

	@Override
	public List<AmendmentContainerDTO> getAmendmentContainersByDocument(
			String arg0) {
        logger.info("**********************************getAmendmentContainersByDocument");
		return null;
	}

//	@Override
//	public List<AmendmentContainerDTO> getAmendmentContainersByDocumentAndPerson(
//			String arg0, String arg1) {
//        logger.info("**********************************getAmendmentContainersByDocumentAndPerson");
//		return null;
//	}

    @GET
    @Path("/document/{documentID}/{personID}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Override
    public List<AmendmentContainerDTO> getAmendmentContainersByDocumentAndPerson(@PathParam("documentID") String documentID, @PathParam("personID") String personID) {
//        final Person person = PersonRepository.findByPersonID(personID);
//        if (person != null) {
//            final Document document = DocumentRepository.findByDocumentID(documentID);
//            if (document != null) {
//                final List<AmendmentContainer> amendmentContainers = AmendmentContainerRepository.findByDocumentAndPerson(document, person);
//                final List<AmendmentContainerDTO> amendmentContainerDTOs = new ArrayList<AmendmentContainerDTO>();
//                //amendmentContainerAssembler.assembleDtos(amendmentContainerDTOs, amendmentContainers, getConvertors(), new DefaultDSLRegistry());
//                return amendmentContainerDTOs;
//            }
//        }

        logger.info("**Amendment Document ID: " + documentID);
        logger.info("**Amendment Person ID: " + personID);

        List<AmendmentContainerDTO> amendmentContainerDtos = new ArrayList<AmendmentContainerDTO>();
        List<AmendmentContainer> amendmentContainers = AmendmentContainerRepository.getAmendmentListByDocumentId(documentID);

        amendmentContainerAssembler.assembleDtos(amendmentContainerDtos, amendmentContainers, getConvertors(), new DefaultDSLRegistry());
        return amendmentContainerDtos;
    }

    @POST
    @Path("/save")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML, MediaType.APPLICATION_XML})
    @Override
    public AmendmentContainerDTO save(final @PathParam("amendmentContainerDTO") AmendmentContainerDTO amendmentContainerDTO) {

//        find existing
//        AmendmentContainer amendmentContainer = AmendmentContainerRepository.findByAmendmentContainerID(amendmentContainerDTO.getAmendmentContainerID());
//        if (amendmentContainer == null) amendmentContainer = new AmendmentContainer();
//        AmendmentContainer amendmentContainer = new AmendmentContainer();
//        amendmentContainerAssembler.assembleEntity(amendmentContainerDTO, amendmentContainer, getConvertors(), new DefaultDSLRegistry());

        logger.info("**Amendment Container Id: "+amendmentContainerDTO.getAmendmentContainerID());
        logger.info("**Amendment Document Id: "+amendmentContainerDTO.getDocumentID());
        logger.info("**Amendment Language Iso: "+amendmentContainerDTO.getLanguageISO());
        logger.info("**Amendment Person Id: "+amendmentContainerDTO.getPersonID());
        logger.info("**Amendment Revision Id: "+amendmentContainerDTO.getRevisionID());

        AmendmentContainerRepository.save(amendmentContainerDTO);
//        amendmentContainerAssembler.assembleDto(amendmentContainerDTO, amendmentContainer, getConvertors(), new DefaultDSLRegistry());
        return amendmentContainerDTO;
    }

    private Map<String, Object> getConvertors() {
        return new HashMap<String, Object>() {
            {
                put("documentIDConvertor", documentIDConvertor);
                put("personIDConvertor", personIDConvertor);
                put("amendableWidgetReferenceConvertor", amendableWidgetReferenceConvertor);
                put("amendmentActionConvertor", amendmentActionConvertor);
            }
        };
    }

}
